const express = require('express');
const router = express.Router();
const actionCtrl = require('../controllers/actions');
const multer = require('../middleware/multer-config')

router.post('/upload', multer, actionCtrl.uploadPicture);

router.get('/manual', actionCtrl.manualUploadView);

router.get('/screenshot/:id', actionCtrl.getPicture);

module.exports = router;