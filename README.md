# MSPR API

## Récupération du projet

Afin de récupérer le projet il suffit d'effectuer la commande `git clone https://gitlab.com/dev1656/mspr_api.git`

## Installation du projet

L'installation du projet s'effectue avec la commande `npm install`

## Lancement du projet

Pour lancer le projet il faut utiliser la commande `npm run dev`
