exports.uploadPicture = async (req, res, next) => {
    console.log(`GOULOUGOULOU`)
    console.log(`${req.protocol}://${req.get('host')}/public/img/${req.file}`)
    //let imageUrl = `${req.protocol}://${req.get('host')}/public/img/${req.file.filename}`

    res.status(201).json({ name: req.file.filename })
};

exports.manualUploadView = async (req, res, next) => {
    console.log('//// *** MANUAL UPLOAD *** ////')
    res.render('view')
};

exports.getPicture = async (req, res, next) => {
    console.log('//// *** SCREENSHOT *** ////')

    res.render('screenshot', {image : req.params.id})
};